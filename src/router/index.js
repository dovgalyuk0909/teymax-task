import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/components/Home.vue'

import List from '@/components/infoFull.vue'
import E404 from '@/components/E404.vue'

Vue.use(VueRouter)

const routes = [

  {
    path: '',
    name: 'home',
    component: Home
  },
  {
    path: '/location/:id',
    name: 'location',
    component: List,
  },
  {
    path: '*',
    component: E404
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
