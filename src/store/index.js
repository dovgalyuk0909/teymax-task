import Vue from 'vue'
import Vuex from 'vuex'
import dataApi from './modules/dataApi'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    dataApi
  }
})
