export default {
    state: {
        dataWeather: [],
        weatherForFiveDays: [],
        weatherForFiveDaysIs: true,
        currentDate: '',
        cityName: '',
        cityList: [],
        requestIs: false
    },
    mutations: {
        updateDataWeather(state,data) {
            data.sunrise = new Date(data.sys.sunrise).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
            data.sunset = new Date(data.sys.sunset).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: false })
            state.dataWeather = data
            state.requestIs = true;
        },
        updateDate(state) {
            const date = new Date();
            state.currentDate = date.toDateString()
        },
        updateWeatherForFiveDays(state,data) {
            state.weatherForFiveDays = data.list.filter((item) => {
                let date = item.dt_txt.split(' ')
                if(date[1] === '00:00:00') {
                    item.sunrise = new Date(data.city.sunrise).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
                    item.sunset = new Date(data.city.sunset).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: false })

                    item.dt_txt = date[0]
                    return item
                }
            })
        },
        changeWeatherForFiveDaysIs(state, count) {
            state.weatherForFiveDaysIs = count
        },
        clearAll(state) {
            state.requestIs = false
        },
        addCityName(state, name) {
            state.cityName = name
        },
        addToCityList(state,name) {
            name = name.toUpperCase()
            const cityNameIs = state.cityList.some(city => name === city)
            if(cityNameIs) {
                return
            } else {
                state.cityList.push(name)
            }
        }
    },
    actions: {
        setLocation({commit,dispatch},cityName) {
            commit('changeWeatherForFiveDaysIs', false)
            commit('addCityName', cityName)
            let request = `http://api.openweathermap.org/data/2.5/weather?q=${cityName}&units=metric&APPID=c0b2b721127d5a66256668aa85fa1fd7`
            const promise = dispatch('fetchDate',request)
            promise
                .then(
                    data => commit('updateDataWeather', data)
                )
        },
        getDataWeatherFull({commit,dispatch}) {
            commit('changeWeatherForFiveDaysIs', true)
            let cityName = this.state.dataApi.dataWeather.name
            let request = `http://api.openweathermap.org/data/2.5/forecast?q=${cityName}&&units=metric&APPID=c0b2b721127d5a66256668aa85fa1fd7`
            const promiseData = dispatch('fetchDate',request)
            promiseData
                .then(
                    data => commit('updateWeatherForFiveDays', data)
                )
        },
        async fetchDate({commit},request) {
            let promise = await fetch (request);
            if(promise.ok) {
              let data = await promise.json()
              commit('updateDate')
              return data
            } else {
              console.error(promise.status)
            }
        },
        getLocation({commit, dispatch}) {
            commit('clearAll')
            let request = 'http://api.sypexgeo.net/json/'
            const promiseData = dispatch('fetchDate',request)
            promiseData
                .then(
                    data => dispatch('setLocation',data.city.name_en) 
                )
        },
        addLocations({commit},name) {
            commit('addToCityList',name)
        }
    },
    getters: {
        getDataWeather(state) {
            return state.dataWeather
        },
        getRequestCount(state) {
            return state.requestIs
        },
        getDate(state) {
            return state.currentDate
        },
        getWeatherForFiveDays(state) {
            return state.weatherForFiveDays
        },
        getWeatherForFiveDaysIs(state) {
            return state.weatherForFiveDaysIs
        },
        getCityName(state) {
            return state.cityName
        },
        getCityList(state) {
            return state.cityList
        }
    }
}